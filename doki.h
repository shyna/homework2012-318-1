﻿/**Данная библиотека предназначена для работы с 
* объектами типа <tt>Long</tt>
*/
#ifndef LONG_H
#define LONG_H

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;
/**
* @author Zhailauova Shynar
* <p>
* Класс служит для создания объектов - длинных чисел
*/
class Long {
    
    int length, sign, del;
    int size_long, size_bin;
    int *array, *b_mod, *d_mod ;
    int *binary, *decimal;
    
    public:
/**Конструктор по умолчанию
* @return         объект класса <tt>Long</tt>, равный нулю
*/
    Long ();


/**Конструктор преобразования
* @param chislo   строка, представляющая длинное целое
* @return         объект класса <tt>Long</tt>
*/
    Long (string chislo);


/**Перегрузка операции сложения двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>, <tt>plus()</tt>, 
* <tt>minus()</tt>, <tt>todec()</tt>, <tt>turn_dec()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         сумму чисел <tt>a</tt> и <tt>b</tt>
* @see            compare()
* @see            plus()
* @see            minus()
* @see            todec()
* @see            turn_dec()
*/
    friend Long operator + ( const Long& a, const Long & b);


/**Перегрузка операции разности двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>, <tt>plus()</tt>, 
* <tt>minus()</tt>, <tt>todec()</tt>, <tt>turn_dec()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         разность чисел <tt>a</tt> и <tt>b</tt>
* @see            compare()
* @see            minus()
* @see            plus()
* @see            todec()
* @see            turn_dec()
*/
    friend Long operator - ( const Long& a, const Long& b);


/**Перегрузка операции умножения двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>, <tt>multiply()</tt>, 
* <tt>todec()</tt>, <tt>turn_dec()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         произведение чисел <tt>a</tt> и <tt>b</tt>
* @see            compare()
* @see            multiply()
* @see            todec()
* @see            turn_dec()
*/
	friend Long operator * ( const Long& a, const Long& b);


/**Перегрузка операции целочисленного деления двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>, <tt>multiply()</tt>, 
* <tt>turn_dec()</tt>, <tt>todec()</tt>, <tt>turn_dec()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         результат целочисленного деления числа <tt>a</tt> на <tt>b</tt>
* @see            compare()
* @see            division()
* @see            turn()
* @see            todec()
* @see            turn_dec()
*/
	friend Long operator / ( const Long& a, const Long& b);


/**Перегрузка операции остатка от деления двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>, <tt>multiply()</tt>, 
* <tt>turn_dec()</tt>, <tt>minus()</tt>, <tt>todec()</tt>, <tt>turn_dec()</tt>,
* <tt>division()</tt>
* @param a        первый операнд
* @param b        второй операнд
* @return         остаток деления числа <tt>а</tt> на <tt>b</tt>
* @see            compare()
* @see            division()
* @see            multiply()
* @see            turn()
* @see            minus()
* @see            todec()
* @see            turn_dec()
*/
	friend Long operator % ( const Long& a, const Long& b);


/**Перегрузка операции сравнения > двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt>a</tt> больше <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            compare()
*/
	friend bool operator > ( const Long& a, const Long& b);


/**Перегрузка операции сравнения < двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt></tt>a меньше <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            compare()
*/
	friend bool operator < ( const Long& a, const Long& b);


/**Перегрузка операции сравнения <= двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt>a</tt> меньше или равно <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            compare()
*/
	friend bool operator <= ( const Long& a, const Long& b);


/**Перегрузка операции сравнения >= двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt>a</tt> больше или равно <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            compare()
*/
	friend bool operator >= ( const Long& a, const Long& b);


/**Перегрузка операции сравнения == двух длинных чисел
* <p>
* В ее реализации так же использются <tt>compare()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt>a</tt> равно <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            compare()
*/
	friend bool operator == ( const Long& a, const Long& b);


/**Перегрузка операции сравнения != двух длинных чисел
* <p>
* В ее реализации так же использются <tt>operator ==()</tt>.
* @param a        первый операнд
* @param b        второй операнд
* @return         <code>true</code> если <tt>a</tt> не равно <tt>b</tt>;
*                 <code>false</code> в другом случае;
* @see            operator ==()
*/
	friend bool operator != ( const Long& a, const Long& b);

/**Перегрузка операции чтения из потока длинного целого
* @param num      входной поток
* @param b        вводимое число
* @return         поток
*/
	friend ostream& operator << ( ostream & num, const Long& b);


/**Перегрузка операции вывода в поток длинного целого
* @param num      выходной поток
* @param b        выводимое число
* @return         объект типа <tt>Long</tt>
*/
	friend Long& operator >> ( istream & num,  Long& b);
          
};


/** Эта функция переводит длинное целое из десятичной системы
* исчисления в двоичную систему исчисления.
*
* @param dec      длинное целое, представленное массивом 
*                 в десятичном виде               
* @param len_dec  длина массива <tt>dec</tt>
* @param len_bin  длина массива <tt>bit</tt>
* @param bit      длинное целое, представленное массивом 
*                 в двоичном виде
* @return         длинное целое, представленное массивом 
*                 в двоичном виде
*/
int*tobin(int* dec, int len_dec, int &len_bin, int *bit);

/**Эта функция обратная функции {@link tobin},она 
* переводит длинное целое из двоичной системы
* исчисления в десятичную систему исчисления.
*
* @param binar    длинное целое, представленное массивом 
*                 в двоичном виде               
* @param bin_bin  длина массива <tt>binar</tt>
* @param dec_dec  длина массива <tt>result</tt> 
* @param result   длинное целое, представленное массивом 
*                 в десятичном виде
* @return         длинное целое, представленное массивом 
*                 в десятичном виде
*/
int* todec(int *binar, int bin_bin, int &dec_dec, int* result);

/**Эта функция переворачивает запись числа в массиве,то есть 
* начальные елементы становятся конечными, а конечные -
* начальными.
*
* @param g        массив, порядок элементов которых нужно изменить               
* @param len      длина массива <tt>g</tt>
* @return         массив с новым порядком элементов
*/
int* turn(int *g, int len);

/**Эта функция похожа на  функцию {@link turn},только  
* в отличие от фунции {@link turn}, при изменении порядка
* следования элементов в массиве не трогает данный массив,
* а создает новый.
*
* @param turn_dec массив, порядок элементов которых нужно изменить               
* @param len      длина массива <tt>turn_dec</tt>
* @param res      новый массив с измененным порядком элементов
* @return         массив с новым порядком элементов
*/
int* turn_dec(int *g, int len, int *res);

/**Эта функция убирает незначащие нули впереди числа.
*
* @param g        число, представленное массивом в двоичном виде                 
* @param len      длина массива <tt>g</tt>
* @param pes      колличество незначащих нулей в массиве <tt>g</tt>
* @return         колличество незначащих нулей в массиве <tt>g</tt>
*/
int cutzero(int* g, int len, int pen);

/**Эта функция сравнивает два длинных целых числа,
* представленных массивами в десятичном виде, при  
* этом не учитывая их знаков.
*
* @param long_1   первое число, представленное массивом 
*                 в десятичном виде               
* @param len_1    длина массива <tt>long_1</tt>
* @param long_2   втрое число, представленное массивом 
*                 в десятичном виде
* @param len_2    длина массива <tt>long_2</tt>
* @param res      результат сравнения двух чисел
* @return         <code>0</code> если два числа равны; 
*                 <code>1</code> если первое число больше второго; 
*                 <code>2</code> если второе число больше первого
*/
int compare(int * long_1, int len_1, int *long_2, int len_2, int res);

/**Эта функция  похожа на функция {@link compare()}, она
* сравнивает два длинных целых в двоичной записи,
* при этом не учитывая их знаков.Также эта функция отличается
* от функции {@link compare()} тем, что на входе задаются
* позиции начала {@link fir} и конца {@link sec} массива,
* представляющего первое число. Это связано с тем, что в 
* массиве, представляющем первое число, могут содержаться незначащие нули.
*
* @param fir      номер элемента массива <tt>v</tt>, с которого начинается
*                 сравнение
* @param v        первое число, представленное массивом 
*                 в двоичном виде  
* @param sec      номер элемента массива <tt>v</tt>, которым заканчивается
*                 сравнение
* @param s        второе число, представленное массивом 
*                 в двоичном виде 
* @param pen      длина массива <tt>s</tt>
* @param res      результат сравнения двух чисел
* @return         <code>0</code> если два числа равны
*                 <code>1</code> если первое число больше второго
*                 <code>2</code> если второе число больше первого
*/
int comp_bin(int fir, int *v, int sec, int *s, int pen, int res);

/**Эта функция складывает два длинных целых в двоичной  
* записи, при этом знаки у этих чисел одинаковы.
*
* @param bin_1    первое число, представленное массивом 
*                 в двоичном виде               
* @param len_1    длина массива <tt>bin_1</tt>
* @param bin_2    второе число, представленное массивом 
*                 в двоичном виде
* @param len_2    длина массива <tt>bin_2</tt>
* @param res_len  длина массива <tt>res_b</tt>
* @param res_b    массив, представляющий сумму 
*                 данных чисел в двоичной системе
* @return         массив, представляющий сумму 
*                 данных чисел в двоичной системе
*/
int* plus(int *bin_1, int len_1, int *bin_2, int len_2, int &res_len, int * res_b);


/**Эта функция вычитает из первого числа  второе число. 
* Числа представлены в виде массива в двоичном виде: соответственно 
*{@link b} и {@link g}.
*
* @param b        первое число, представленное в виде массива                
* @param lok      длина массива <tt>b</tt>
* @param g        второе число, представленное в виде массива
* @param pok      длина массива <tt>g</tt>
* @param mok      длина массива <tt>z</tt>
* @param z        массив, представляющий разность данных чисел
*                 в двоичном виде
* @return         массив, представляющий разность данных чисел
*                 в двоичном виде
*/
int *minus(int *b, int lok, int *g, int pok, int &mok, int * z);


/**Эта функция выполняет произведение двух чисел, представленных  
* в виде массивов в двоичном виде: соответственно 
*{@link f} и {@link m}.
* Так же в реализации этой функции участвуют функции <tt>cutzero()</tt>. 
*
* @param f        первое число, представленное в виде массива               
* @param lok      длина массива <tt>f</tt>
* @param m        второе число, представленное в виде массива
* @param pok      длина массива <tt>m</tt>
* @param mok      длина массива <tt>z</tt>
* @param z        массив, представляющий разность данных чисел
*                 в двоичном виде
* @return         массив, представляющий разность данных чисел
*                 в двоичном виде
* @see            cutzeto()
*/
int *multiply(int *f, int lok, int *m, int pok, int &mok, int * z);


/**Эта функция выполняет целочисленное деление двух чисел, представленных  
* в виде массивов в двоичном виде: соответственно 
* {@link f} и {@link m}.
* <p>
* Так же в реализации этой функции участвуют функции <tt>cutzero()</tt>,
* <tt>comp_bin()</tt>.
*
* @param f        первое число, представленное в виде массива               
* @param lok      длина массива <tt>f</tt>
* @param m        второе число, представленное в виде массива
* @param pok      длина массива <tt>m</tt>
* @param mok      длина массива <tt>din</tt>
* @param din      массив, представляющий результат целочисленного  
*                 деления первого числа на второе в двоичном виде
* @return         массив, представляющий результат целочисленного  
*                 деления первого числа на второе в двоичном виде
* @see            comp_bin()
* @see            cutzero()
*/
int* division(int *f, int lok, int *m, int pok, int &mok, int* din);

#endif
