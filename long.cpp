#include "doki.h"

using namespace std;

int* tobin (int* dec, int len_dec, int &len_bin, int *bit) {
           
    int element[] = {0,0,0,0,0,0};
    int k = 0, l = 0, flag = 0, len = 6;
    int i, j, razriad, temp_1;
    int el_ost, el_int, ost, len_num, size;
      
    bit = NULL;
    i = 1; j = 0; 
    size = len_dec;
    
    while (i <= size) {
        while (dec[i-1] > 0) {
            k = i;       
            while (k < size) {
                razriad = 10000;
                temp_1 = ((dec[k-1] % 64) * razriad + dec[k]);                        
                dec[k-1] = dec[k-1] / 64;
                dec[k] = temp_1;
                k++;
            }     
                  
            ost = dec[--k] % 64;
            dec[k] = dec[k] / 64;

            el_int = ost;
            el_ost = ost;
            k = 0;
            
            if (ost != 0) {
                while (el_int != 1) {
                    el_ost = el_int % 2;
                    el_int = el_int / 2;
                    element[k++] = el_ost;
                }
                element[k] = 1;
            }
            
            for (j = 0; j < len; ++j) {
                bit = (int*)realloc(bit, (++l)*sizeof(int));
                bit[l-1] = element[j];
            }
            
            for (j = 0; j < len; ++j)
                element[j] = 0;
        }   
        i++;
          
    }
       
    len_bin = l;             
    return bit;        
}
               
//---------------------------------todec------------------------------------------


int* todec (int *binar, int bin_bin, int &dec_dec, int* result) {
    int k, i, j, temp, dop;
    int *dec = NULL;
        
    k = 0;
    temp = 0;
    dec = (int *)realloc(dec, (++k)*sizeof(int));
     
    dec[k-1] = 0;
    for (i = bin_bin - 1; i >= 0; --i) {
        dop = 0;
        for ( j = 0; j < k; ++j) {              
            temp = dec[j] * 2 + dop;
            dec[j] = temp % 10000;
            dop = temp / 10000;
        }
        if (dop > 0) {
            dec = (int *)realloc(dec, (++k)*sizeof(int));
            dec[k-1] = dop;
        }
       
        dec[0] = dec[0] + binar[i];
    }

    dec_dec = k;
            
    return dec;
}
//----------------------------turn--------------------------------------------
int* turn (int *g, int len) {
    int i, temp, mid, k;
     
    if( (len % 2) == 0) {
        mid = len / 2;
        k = mid - 1;
    } else if ((len % 2) == 1) {
        mid = len / 2 + 1;
        k = mid - 2;
    }
    for (i = mid; i < len; ++i) {
        temp = g[i];
        g[i] = g[k];
        g[k--] = temp;
    }
        
    return g;
}

//--------------------------------turn_dec-------------------------------------
int* turn_dec (int *g, int len, int* res) {
    int i, temp, mid, k;
    res = NULL;
    i = 0;
    while (i != len) {
        res = (int*)realloc(res, (++i)*sizeof(int));
        res[i-1] = g[i-1];
    }
     
     if ((len % 2) == 0) {
         mid = len / 2;
         k = mid - 1;
     } else if ((len % 2) == 1) {
         mid = len / 2 + 1;
         k = mid - 2;
     }
     for (i = mid; i < len; ++i) {
         temp = res[i];
         res[i] = res[k];
         res[k--] = temp;
     }
     i = 0;
      
     return res;
     
}
//----------------------------cutzero------------------------------------------
int cutzero (int* g, int len, int pen){
    int i = len - 1;
    pen = 0;
    while (i > 0) {
       if (g[i--] == 0) {
           pen++;
       } else return pen;
    }
    return pen;
}
//-------------------COMPARE---------------------------------------------------
int compare (int * long_1, int len_1, int *long_2, int len_2, int res) {
    int i = 0, j = 0;
       
    if (len_1 > len_2) {
        while (long_1[i++] == 0)
            len_1--;
    }
    else if (len_1 < len_2) {
        while (long_2[j++] == 0)
            len_2--;
    }
    if (len_1 > len_2)           
        res = 1;
    else if (len_1 < len_2)
        res = 2;
    else if (len_1 == len_2) {
        while ( i < len_1) {
            if (long_1[i] > long_2[j]) {
                res = 1;
                return res;
            }
            else if (long_1[i] < long_2[j]) {
                res = 2;
                return res;
            }
            i++; j++;
        }
        res = 0;
    }
    return res;
}
//-------------------COMPARE_BIN----------------------------------------------
int comp_bin (int fir,int *v, int sec, int *s, int pen, int res) {
    int i, len, rut, j;
    i = fir;
    while (v[i] == 0) {
        fir--;
        i--;
    }
    len = fir - sec + 1;
      
    j = pen - 1;
    if (len > pen)
        res = 1;
    else if (len < pen)
        res = 2;
    else if (len == pen) {
        for(i = fir; i >= sec; --i) {
            if (v[i] > s[j]) {
                res = 1;
                return res;
            }
            else if (v[i] < s[j]) {
                res = 2;
                return res;
            }
            j--;
        }
        res = 0;
    }
    return res;
}

//--------------------------PLUS--------------------------------------------
int* plus (int *bin_1, int len_1, int *bin_2, int len_2, 
           int &res_len, int * res_b) {
    int i, j;
    unsigned int middle[len_1+2], dop;
    res_b = NULL;
     
    dop = 0;
    i = 0; j = 0;
    while (j < len_1) {
        while (i < len_2) {                
            if ((bin_1[i] == 1) && (bin_2[i] == 1)) {
                middle[i] = 0;
                middle[i] = middle[i] + dop;
                dop = 1;
            } else if ((bin_1[i] == 0) && (bin_2[i] == 0)) {
                middle[i] = 0;
                middle[i] = middle[i] + dop;
                dop =0;
            } else{
                middle[i] = bin_1[i] + bin_2[i];
                if ((middle[i] == 1) && (dop == 1))
                    middle[i] = 0;
                else middle[i] = middle[i] + dop;
            }
            i++;
            j = i;
        }
           
        if (len_1 > len_2) {
            if (dop == 1) {
                if (bin_1[j] == 1)
                    middle[j] = 0;
                else {
                    middle[j] = bin_1[j] + dop; 
                    dop = 0;
                }
            }
            else middle[j] = bin_1[j];
        }
        else {
             middle[j] = dop;
             dop = 0;
        } 
                              
        j++;
    }
     
    res_len = j;
 
    j = 0;
    while (j != res_len) {
        res_b = (int *) realloc(res_b, (++j)* sizeof(int));
        res_b[j-1] = middle[j-1];
           // cout << res_b[j-1]<< endl;
    }    
    return res_b;
}

//------------------------minus-------------------------------------------------

int *minus (int *b, int lok, int *g, int pok, int &mok, int * z) {
    int min[lok], i = 0, j;  
    z = NULL;  
    i = 0;
    while (i < pok) {
        if (b[i] >= g[i])
            min[i] = b[i] - g[i];
        else if (b[i] < g[i]) {
            j = i;
            while (b[i] < g[j])
                i++;
            --b[i--];
            while (i >= j)
                ++b[i--];
            ++b[j];
            i = j;
            min[i] = b[i] - g[i];
        }
        i++;
    }
    while (i < lok) {
        min[i] = b[i];
        i++;
    }
    j = 0; mok = lok;
    while (j != lok) {
        z = (int *) realloc(z, (++j)* sizeof(int));
        z[j-1] = min[j-1];
    }    
    return z;
} 
//---------------------------MULTIPLY---------------------------------------

int *multiply (int *f, int lok, int *m, int pok, int &mok, int * z) {
    int i, j, k, dop, temp, cut;
    z = NULL;
    cut = cutzero ( m, pok, cut);
    for (i = 0; i < pok-cut; ++i) {
        k = i; dop = 0;
        for (j = 0; j < lok; ++j) {
            temp = f[j] * m[i];
                  
            if (i == 0) { 
                z = ( int* ) realloc(z, (++k)* sizeof(int));
                z[k-1] = 0;
            } else ++k;
                        
            if ((temp == 1) && (z[k-1] == 1)) {
                z[k-1] = 0;
                z[k-1] = z[k-1] + dop;
                dop = 1;
            } else if ((temp == 0) && (z[k-1] == 0)) {
                z[k-1] = 0;
                z[k-1] = z[k-1] + dop;
                dop =0;
            } else{
                z[k-1] = temp + z[k-1];
                if ((z[k-1] == 1) && (dop == 1))
                    z[k-1] = 0;
                else z[k-1] = z[k-1] + dop;
            }
        }
        z = ( int* ) realloc(z, (++k)*sizeof(int));
        z[k-1] = dop;
    }
    mok = k;
    return z;      
}             
//-----------------------DIVISION-----------------------------------------------
int* division (int *f, int lok, int *m, int pok, int &mok, int* din) {
    int i, j, k, l, dop, temp, cut, put, flag, q;
    int need, nlok, npok, p, min[lok];
      
    q = 0;
    put = cutzero ( m, pok, put);
    cut = cutzero ( f, lok, cut);
     
    nlok = lok - cut;
    npok = pok - put;
    din = NULL;
    flag = 1;
      
    need = comp_bin (nlok-1, f,0, m, npok, need);
    if (need == 2) {
        din = (int*)realloc(din, (++q)* sizeof(int));
        din[q-1] = 0;
        mok = q;
        return din;
    }
           
    for (i = 0; i < lok; ++i)
        min[i] = f[i];
        
    i = nlok-1;
       
    need = comp_bin (i, min, i-(npok-1), m, npok, need);
    p = i - npok + 1;
    k = p;
    while (i > 0) {
        while ((need == 2) && (k > 0)) {
            k--;
            need = comp_bin (i, min, k, m, npok, need);
            if (need == 2) {
                din = (int*)realloc(din, (++q)* sizeof(int));
                din[q-1] = 0;
            }
            p = k;
        }
            
        j = 0;
        while ((j < npok) && (need != 2)) {            
            if (min[p] >= m[j])
                min[p] = min[p] - m[j];
            else if (min[p] < m[j]) {
                l = p;
                while (min[p] < m[j])
                    p++;
                --min[p--];
                while (p >= l)
                    ++min[p--];
                ++min[l];
                p = l;
                min[p] = min[p] - m[j];
            }
            p++;
            j++;
        }
        if (need != 2) {
            din = (int*) realloc(din, (++q)* sizeof(int));
            din[q-1] = 1;
            cut = cutzero (min, lok, cut);
            nlok = lok - cut;
            i = nlok - 1;
            if ((i < (p - npok - 1)) && (i > 0)) 
                i = p - npok - 1;        
        } else if(need == 2)
            i = -1;
        need = 2;
        k = p - npok;    
    }
    while (k > 0) {
        din = (int*)realloc(din, (++q)* sizeof(int));
        din[q-1] = 0;
        k--;
    }
    if (npok == 1)
        din[q-1] = 1;
       
    mok = q;

    return din;
}  
//----------------------------------------------------------------------------

Long::Long() { array = 0; }           
//-----------------------------------------------------------------------------

Long::Long (string a) {
      int n, i, pos, size, j, size_dec;
      size_t part;
      char temp[5];
        
      if (a[0] == '-') {
          sign = 1;   
          n = (a.length() - 1) / 4;
          length = a.length() - 1;
          pos = 1;
          size = n + 1;
      } else{
          sign = 0;
          length = a.length();
          n = a.length() / 4;
          size = n + 1;
          pos = 0;
      }
      if (length == 4 * n)
          --size;
                     
      int middle[size];
      i = 0; j = 0;
      if (length != 4 * n) {
          part = a.copy(temp, (length - 4 * n), pos);
          temp[part] = '\0';
          middle[0] = atoi(temp);
          j++;
        }
      
      pos = (length - 4*n) + pos;
      for (int i = j; i < size; ++i) {
          part = a.copy(temp, 4, pos);
          temp[part] = '\0';
          middle[i] = atoi(temp);
          pos = pos + 4;  
      }
      
      array = NULL; 
      i = 0;
      while (i != size) {
          array = (int *)realloc(array, (++i)*sizeof(int));
          array[i-1] = middle[i-1];   
      }
      size_long = size;
      
      binary = :: tobin( middle, size, size_bin, binary);
      decimal = :: todec( binary, size_bin, size_dec,decimal);
}
 
 //--------------------------------------------------------------------------
Long &operator >> (istream & num, Long &N) {     
    string str;
    num >> str;
    Long D(str);
    N = D;
    return N;
}
//-----------------------------------------------------------------------------
ostream &operator << (ostream &num, const Long &D) {
    if (D.sign == 1)
        num << "-";
    num << D.decimal[D.size_long - 1];
    for (int i = D.size_long - 2; i >= 0; --i ) {
        if ((D.decimal[i] < 100) && (D.decimal[i] >= 10))
            num << "00";
        else if ((D.decimal[i] < 1000) && (D.decimal[i] >= 100))
            num << "0";
        else if ((D.decimal[i] >= 0) && (D.decimal[i] < 10))
            num << "000";
        num << D.decimal[i];
    }
    num << endl;
    return num;
}
//------------------------------------------------------------------------------
Long operator + (const Long &A, const Long &B) {
    int res, *r_bin, *r_dec,rl_bin,rl_dec;
    Long C;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
                     
    if (A.sign == B.sign) {
        C.sign = A.sign;
        if (res == 1 || res == 0)
            C.binary = :: plus(A.binary, A.size_bin, B.binary, 
                               B.size_bin, rl_bin, r_bin);
        else if (res == 2)
            C.binary = :: plus(B.binary, B.size_bin, A.binary, 
                               B.size_bin, rl_bin, r_bin);
    } else if (A.sign != B.sign) {
        if (res == 1 || res == 0)
            C.binary = :: minus(A.binary, A.size_bin, B.binary, 
                                B.size_bin, rl_bin, r_bin);
        else if (res == 2) {
            C.sign = B.sign;
            C.binary = :: minus(B.binary, B.size_bin, A.binary, 
                                A.size_bin, rl_bin, r_bin);
        }
    }
    if (res == 1) C.sign = A.sign;
    if (res == 0) C.sign = 0;
                    
    C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
    C.size_long = rl_dec;
    C.size_bin = rl_bin;
    C.array = turn_dec(C.decimal, rl_dec, C.array);
                 
    return C;
}
//------------------------------------------------------------------------------
Long operator - (const Long &A, const Long &B) {
    int res, *r_bin, *r_dec,rl_bin,rl_dec;
    Long C;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
    if (A.sign == B.sign) {
        C.sign = A.sign;
        if (res == 1 || res == 0)
            C.binary = :: minus(A.binary, A.size_bin, B.binary, 
                                B.size_bin, rl_bin, r_bin);
        else if (res == 2)
            C.binary = :: minus(B.binary, B.size_bin, A.binary, 
                                B.size_bin, rl_bin, r_bin);
    }
                     
    else if (A.sign != B.sign) {
                           
        if (res == 1 || res == 0)
            C.binary = :: plus(A.binary, A.size_bin, B.binary, 
                               B.size_bin, rl_bin, r_bin);
        else if (res == 2) {
            C.sign = B.sign;
            C.binary = :: plus(B.binary, B.size_bin, A.binary, 
                               B.size_bin, rl_bin, r_bin);
        }
        if (B.sign == 1) C.sign = 0;
        else C.sign = 1;
    }  
                     
    C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
    C.size_long = rl_dec;
    C.size_bin = rl_bin;
    C.array = turn_dec(C.decimal, rl_dec, C.array);
    return C;
}
//------------------------------------------------------------------------------
Long operator * (const Long &A, const Long &B) {
    int res, *r_bin, *r_dec,rl_bin,rl_dec;
    Long C;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
                     
    if (res==1 || res==0)
        C.binary = :: multiply(A.binary, A.size_bin, B.binary, B.size_bin, rl_bin, r_bin);
    else if(res==2)
        C.binary = :: multiply(B.binary, B.size_bin, A.binary, A.size_bin, rl_bin, r_bin);
                     
    if (A.sign != B.sign) C.sign = 1;
    if (A.sign==B.sign) C.sign = 0;
                     
    C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
    C.size_long = rl_dec;
    C.size_bin = rl_bin;
    C.array = turn_dec(C.decimal, rl_dec, C.array);
    return C;
}
//------------------------------------------------------------------------------
Long operator / (const Long &A, const Long &B){
    int res, *r_dec, *din, rl_bin,rl_dec, hop;
    int i;
    Long C;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
                     
    if (res==1 || res==0) {
        C.binary = :: division(A.binary, A.size_bin, B.binary, 
                               B.size_bin, rl_bin, din);
        if (A.sign != B.sign) C.sign = 1;
        if (A.sign==B.sign) C.sign = 0;
    }
    else if(res==2){
        C.binary = :: division(A.binary, A.size_bin, B.binary, 
                               B.size_bin, rl_bin, din);
        C.sign = 0;
    }
                    
    C.binary = :: turn(C.binary, rl_bin);
    C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
    C.size_long = rl_dec;
    C.size_bin = rl_bin;
    C.array = turn_dec(C.decimal, rl_dec, C.array);
    return C;
}
//------------------------------------------------------------------------------
Long operator % (const Long &A, const Long &B) {
    int res, *r_dec, *din, rl_bin,rl_dec, *r_bin, hop;
    int i;
    Long C;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
                      
    if (res == 1 || res == 0) {
        C.binary = :: division(A.binary, A.size_bin, B.binary, B.size_bin, rl_bin, din);
        C.binary = :: turn(C.binary, rl_bin);
        C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
        C.array = :: turn(C.decimal, rl_dec);
                                
        hop = :: compare(C.array, rl_dec, B.array, B.size_long, hop);
                     
            if (hop == 1 || hop == 0)
                C.binary = :: multiply(C.binary, rl_bin, B.binary, 
                                       B.size_bin, C.size_bin, r_bin);
            else if (hop == 2)
                C.binary = :: multiply(B.binary, B.size_bin, C.binary, 
                                       rl_bin , C.size_bin, r_bin);
                                 
            C.binary = :: minus(A.binary, A.size_bin, C.binary, 
                                C.size_bin, rl_bin, r_bin);
            C.decimal = :: todec(C.binary, rl_bin, rl_dec, C.decimal);
            C.size_long = rl_dec;
            C.size_bin = rl_bin;
            C.array = turn_dec(C.decimal, rl_dec, C.array);                      
            }
    else if (res == 2) {
        i = 0;
        C.binary = turn_dec(A.binary, A.size_bin, C.binary);
        C.binary = turn(C.binary, A.size_bin);
        C.size_bin = A.size_bin;
        C.size_long = A.size_long;
        C.decimal = todec(C.binary, C.size_bin, rl_dec, C.decimal);
        C.array = turn_dec(A.decimal, C.size_long, C.array);                         
    }
                     
    C.sign = 0;  
    return C;
}
//------------------------------------------------------------------------------
bool operator > (const Long &A, const Long &B) {
    int res;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
                     
    if (A.sign == 1 && B.sign == 1) {
        if (res == 1 || res == 0)
            return false;
        else if (res==2)
            return true;
    }
    else if (A.sign == 0 && B.sign== 0) {
        if (res == 1)
            return true;
        else if (res==2 || res==0)
            return false;
    }
    else if (A.sign != B.sign) {
        if (A.sign == 0)
            return true;
        else if (B.sign == 0) {
            return false;
        }
    }
}
//------------------------------------------------------------------------------
bool operator < (const Long &A, const Long &B) {
    int res;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
    if (A.sign == 1 && B.sign == 1) {
    if (res == 1)
        return true;
    else if (res == 2 || res == 0)
        return false;
    }
    else if (A.sign == 0 && B.sign== 0) {
        if (res == 1 || res == 0)
            return false;
        else if (res == 2)
            return true;
    }
    else if (A.sign != B.sign) {
        if (A.sign == 0)
            return false;
        else if (B.sign == 0) {
            return true;
        }
    }
}
//------------------------------------------------------------------------------
bool operator >= (const Long &A, const Long &B) {
    int res;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
    if (A.sign == 1 && B.sign == 1) {
        if (res == 1)
            return false;
        else if (res == 2 || res == 0)
            return true;
    }
    else if (A.sign == 0 && B.sign== 0) {
        if (res == 1 || res == 0)
            return true;
        else if (res == 2)
            return false;
    }
    else if (A.sign != B.sign) {
        if (A.sign == 0)
            return true;
        else if (B.sign == 0) {
            return false;
        }
    }
}
//------------------------------------------------------------------------------
bool operator <= (const Long &A, const Long &B) {
    int res;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
    if (A.sign == 1 && B.sign == 1) {
                           
        if (res == 1 || res == 0)
            return true;
        else if (res == 2)
            return false;
    }
    else if (A.sign == 0 && B.sign== 0) {
                           
        if (res == 1)
            return false;
        else if (res == 2 || res == 0)
            return true;
    }
    else if (A.sign != B.sign) {
        if (A.sign == 0)
            return false;
        else if (B.sign == 0) {
            return true;
        }
    }
}
//-------------------------------------------------------------------------------
bool operator == (const Long &A, const Long &B) {
    int res;
                    
    res = :: compare(A.array, A.size_long, B.array, B.size_long, res);
    if (A.sign == 1 && B.sign == 1) {
                           
        if (res == 0)
            return true;
        else if (res == 2 || res == 1)
            return false;
    }
    else if (A.sign == 0 && B.sign== 0){
                           
    if (res == 0)
        return true;
    else if (res == 2 || res == 1)
        return false;
    }
    else if (A.sign != B.sign) {
        return false;                  
    }               
}
//------------------------------------------------------------------------------
bool operator != (const Long &A, const Long &B) {
    int res;
    if (A == B)
        return false;
    else 
        return true;           
}
