﻿#ifndef TEST_LONG_H
#define TEST_LONG_H

#include"doki.h"
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;
/**Класс <tt>Test_Long</tt>, реализующий набор тестов для объектов класс <tt>Long</tt>
* <p>
* Класс <tt>Test_Long</tt> наследуется от класса <tt>TestFixture</tt>, который
* описан в библиотеке  <tt>CppUnit</tt>
*/
class Test_Long : public TestFixture {
      
    CPPUNIT_TEST_SUITE(Test_Long);
	CPPUNIT_TEST(testADD);
	CPPUNIT_TEST(testSUB);
	CPPUNIT_TEST(testMUL);
	CPPUNIT_TEST(testDIV);
	CPPUNIT_TEST(testMOD);
	CPPUNIT_TEST(testLESS);
	CPPUNIT_TEST(testMORE);
	CPPUNIT_TEST(testLESSEQUAL);
	CPPUNIT_TEST(testMOREEQUAL);
	CPPUNIT_TEST(testNEQUAL);
	CPPUNIT_TEST(testEQUAL);
	CPPUNIT_TEST_SUITE_END();

  public:
/**Эта функция выделяет память для объекта
* класса <tt>Test_Long</tt>.
*/
      void setUp();


/**Эта функция освобождает память, занятую объектом
* класса <tt>Test_Long</tt>.
*/
	  void tearDown();

/**Тест на сложение двух чисел
*/
	  void testADD();

/**Тест на разность двух чисел
*/
	  void testSUB();

/**Тест на произведение двух чисел
*/
	  void testMUL();

/**Тест на целочисленное деление 
* двух чисел
*/
	  void testDIV();

/**Тест на определение остатка от
* целочисленного деления двух чисел
*/
	  void testMOD();

/**Тест на операцию сравнения <
*/
	  void testLESS();

/**Тест на операцию сравнения >
*/
	  void testMORE();

/**Тест на операцию сравнения <=
*/
	  void testLESSEQUAL();

/**Тест на операцию сравнения >=
*/
	  void testMOREEQUAL();

/**Тест на операцию сравнения !=
*/
	  void testNEQUAL();

/**Тест на операцию сравнения ==
*/
	  void testEQUAL();
	  
 
  private:
	  Long *A, *B, *C;

};
	
#endif
