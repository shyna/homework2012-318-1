#ifndef TEST_LONG_H
#define TEST_LONG_H

#include"long.h"
#include <cppunit/extensions/HelperMacros.h>

using namespace CppUnit;

class Test_Long : public TestFixture {
      
    CPPUNIT_TEST_SUITE(Test_Long);
	CPPUNIT_TEST(testADD);
	CPPUNIT_TEST(testSUB);
	CPPUNIT_TEST(testMUL);
	CPPUNIT_TEST(testDIV);
	CPPUNIT_TEST(testMOD);
	CPPUNIT_TEST(testLESS);
	CPPUNIT_TEST(testMORE);
	CPPUNIT_TEST(testLESSEQUAL);
	CPPUNIT_TEST(testMOREEQUAL);
	CPPUNIT_TEST(testNEQUAL);
	CPPUNIT_TEST(testEQUAL);
	CPPUNIT_TEST_SUITE_END();

  public:
      void setUp ();
	  void tearDown ();	
	  void testADD ();
	  void testSUB ();
	  void testMUL ();
	  void testDIV ();
	  void testMOD ();
	  void testLESS ();
	  void testMORE ();
	  void testLESSEQUAL ();
	  void testMOREEQUAL ();
	  void testNEQUAL ();
	  void testEQUAL();
	  
 
  private:
	  Long *A, *B, *C;

};

#endif
