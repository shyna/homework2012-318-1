#ifndef LONG_H
#define LONG_H

#include <iostream>
#include <cstring>
#include <cstdio>
#include <cstdlib>
#include <cmath>

using namespace std;


class Long {
    
      int length, sign, del;
      int size_long, size_bin;
      int *array, *b_mod, *d_mod ;
      int *binary, *decimal;
    
  public:
    Long ();
    Long ( string );
        //Long& operator= ( const Long& );
        friend Long operator+ ( const Long&, const Long & );
	friend Long operator- ( const Long&, const Long& );
	friend Long operator* ( const Long&, const Long& );
	friend Long operator/ ( const Long&, const Long& );
	friend Long operator% ( const Long&, const Long& );
	friend bool operator> ( const Long&, const Long& );
	friend bool operator< ( const Long&, const Long& );
	friend bool operator<= ( const Long&, const Long& );
	friend bool operator>= ( const Long&, const Long& );
	friend bool operator== ( const Long&, const Long& );
	friend bool operator!= ( const Long&, const Long& );
	//friend Long & operator+= ( Long&, const Long& );
	//friend Long & operator-= ( Long&, const Long& );
	//friend Long & operator*= ( Long&, const Long& );
	//friend Long & operator/= ( Long& , const Long& );
	friend ostream& operator<< ( ostream &, const Long& );
	friend Long& operator>> ( istream &,  Long& );
          
};

int*tobin(int* dec, int len_dec, int &len_bin, int *bit);
int* todec(int *binar, int bin_bin, int &dec_dec, int* result);
int* turn(int *g, int len);
int* turn_dec(int *g, int len, int *res);
int cutzero(int* g, int len, int pen);
int compare(int * long_1, int len_1, int *long_2, int len_2, int res);
int comp_bin(int fir,int *v, int sec, int *s, int pen, int res);
int* plus(int *bin_1, int len_1, int *bin_2, int len_2, int &res_len, int * res_b);
int *minus(int *b, int lok, int *g, int pok, int &mok, int * z);
int *multiply(int *f, int lok, int *m, int pok, int &mok, int * z);
int* division(int *f, int lok, int *m, int pok, int &mok, int* din);

#endif
