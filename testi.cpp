#include"testi.h"
#include <cppunit/extensions/TestFactoryRegistry.h>
#include <cppunit/ui/text/TestRunner.h>

using namespace CppUnit;

CPPUNIT_TEST_SUITE_REGISTRATION(Test_Long);

void Test_Long::setUp() {
	
    A = new Long("1");
	B = new Long("11");
	C = new Long("111");
}

void Test_Long::tearDown() {
	
        delete A;
	delete B;
	delete C;
}

void Test_Long::testADD() {
	
        *A = Long("19999678549375673");
	*B = Long("999485738248");
	*C = Long("20000678035113921");
	//cout<< "ia v pervoi iteracii" << endl;
	CPPUNIT_ASSERT( *A + *B == *C );
	
        *A = Long("-987567345876");
	*B = Long("567987890678456");
	*C = Long("567000323332580");
	//cout<< "ia vo vtoroi iteracii"<< endl;
	CPPUNIT_ASSERT( *A + *B == *C );

        *A = Long("-987654321");
	*B = Long("-567789456");
	*C = Long("-1555443777");
	CPPUNIT_ASSERT( *A + *B == *C );
}

void Test_Long::testSUB(){

	*A = Long("56778998767834");
	*B = Long("678098678");
	*C = Long("56778320669156");
	CPPUNIT_ASSERT( *A - *B == *C );
	
        *A = Long("-567098678");
	*B = Long("789678567");
	*C = Long("-1356777245");
	CPPUNIT_ASSERT( *A - *B == *C );

        *A = Long("-789456123");
	*B = Long("-678098123");
	*C = Long("-111358000");
	CPPUNIT_ASSERT( *A - *B == *C );
}

void Test_Long::testMUL() {
	
    *A = Long("8976584539870");
	*B = Long("7895");
	*C = Long("70870134942273650");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );

    *A = Long("7896784512");
	*B = Long("-12547");
	*C = Long("-99080955272064");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );
	
	*A = Long("-98766");
	*B = Long("-9871234568876");
	*C = Long("974942353429607016");
	CPPUNIT_ASSERT( ( *A ) * ( *B ) == *C );
}

void Test_Long::testDIV() {
     
	*A = Long ("1234567890");
	*B = Long ("23466");
	*C = Long ("52610");
        CPPUNIT_ASSERT( *A / *B == *C );
    
        *A = Long ("12345678");
	*B = Long ("-12345678");
	*C = Long ("-1");
        CPPUNIT_ASSERT( *A / *B == *C );
    
        *A = Long ("-9875643298745098");
	*B = Long ("-89765");
	*C = Long ("110016635645");
        CPPUNIT_ASSERT( *A / *B == *C );
    
        *A = Long ("5674390");
	*B = Long ("-786321246843986");
	*C = Long ("0");
        CPPUNIT_ASSERT( *A / *B == *C );
}

void Test_Long::testMOD() {
     
	*A = Long ("1234567890");
	*B = Long ("23466");
	*C = Long ("21630");
        CPPUNIT_ASSERT( *A % *B == *C );
    
        *A = Long ("12345678");
	*B = Long ("-12345678");
	*C = Long ("0");
        CPPUNIT_ASSERT( *A % *B == *C );
    
        *A = Long ("-9875643298745098");
	*B = Long ("-89765");
	*C = Long ("71673");
        CPPUNIT_ASSERT( *A % *B == *C );
    
        *A = Long ("5674390");
	*B = Long ("-786321246843986");
	*C = Long ("5674390");
        CPPUNIT_ASSERT( *A % *B == *C );
}



void Test_Long::testLESS() {
     
	*A = Long("67350985763124709");
	*B = Long("67543129876567890");
	CPPUNIT_ASSERT( *A < *B );
	
	*A = Long("-897655678890");
	*B = Long("9877656732");
	CPPUNIT_ASSERT( *A < *B );
	
	*A = Long("-995432578976");
	*B = Long("-987909984567");
	CPPUNIT_ASSERT( *A < *B );
}

void Test_Long::testMORE() {
	
        *A = Long ("9898754870234");
	*B = Long ("9897964578201");
	CPPUNIT_ASSERT( *A > *B );

        *A = Long ("45");
	*B = Long ("-1000000000");
	CPPUNIT_ASSERT( *A > *B );
	
	*A = Long ("-6794056867483947674834");
	*B = Long ("-6574837656483098976857362");
	CPPUNIT_ASSERT( *A > *B );
}

void Test_Long::testLESSEQUAL() {
     
	*A = Long("987678766");
	*B = Long("11111111111");
	CPPUNIT_ASSERT( *A <= *B );
	
    *A = Long("789678567");
	*B = Long("789678567");
	CPPUNIT_ASSERT( *A <= *B );
	
    *A = Long("-11111116789854704965");
	*B = Long("-986794650532");
	CPPUNIT_ASSERT( *A <= *B );
}

void Test_Long::testMOREEQUAL() {
	
    *A = Long ("17365756867749687745895769847867");
	*B = Long ("97856385t76784837");
	CPPUNIT_ASSERT( *A >= *B );
	
	*A = Long ("9999999999");
	*B = Long ("9999999999");
	CPPUNIT_ASSERT( *A >= *B );
	
	*A = Long ("-869784576365");
	*B = Long ("-876985857685746685678");
	CPPUNIT_ASSERT( *A >= *B );
}

void Test_Long::testNEQUAL() {
	
    *A = Long ("96878847595946857");
	*B = Long ("4567483768574");
	CPPUNIT_ASSERT( *A != *B );
	
	*A = Long ("-16666666666");
	*B = Long ("16666666666");
	CPPUNIT_ASSERT( *A != *B );
	
	*A = Long ("-3456789875568");
	*B = Long ("-65748575748845");
	CPPUNIT_ASSERT( *A != *B );
}

void Test_Long::testEQUAL() {
	
        *A = Long ("876987654765");
	*B = Long ("876987654765");
	CPPUNIT_ASSERT( *A == *B );
	
	*A = Long ("-123456789");
	*B = Long ("-123456789");
	CPPUNIT_ASSERT( *A == *B );
}


int main( int argc, char **argv) {

    CppUnit::TextUi::TestRunner runner;
    CppUnit::TestFactoryRegistry &registry = CppUnit::TestFactoryRegistry::getRegistry();
    runner.addTest( registry.makeTest() );
    runner.run();
    return 0;
}


